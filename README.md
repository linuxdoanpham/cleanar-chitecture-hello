"# cleanar-chitecture-hello" 
# Getting Started
=============================================================================
# Config db: application.properties
    server.port=8080
    server.error.include-message=always
    spring.datasource.url=jdbc:mysql://localhost:3306/clean-architecturea
    spring.datasource.username=root
    spring.datasource.password=admin123
    spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
    spring.jpa.database-platform = org.hibernate.dialect.MySQL8Dialect
    spring.jpa.generate-ddl=true
    spring.jpa.hibernate.ddl-auto = update
# Start Spring 
# Test data
    curl --location --request POST 'http://localhost:8080/user' \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "name": "doan",
        "password": "123456"
    }'
package cleanar.chitecture.demo;


public interface UserInputBoundary {
    UserResponseModel create(UserRequestModel requestModel);
}

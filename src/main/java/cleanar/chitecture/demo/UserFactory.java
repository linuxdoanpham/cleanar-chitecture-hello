package cleanar.chitecture.demo;

interface UserFactory {
    User create(String name, String password);
}

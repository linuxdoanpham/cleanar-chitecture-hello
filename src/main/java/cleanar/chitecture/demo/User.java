package cleanar.chitecture.demo;

interface User {
  
    boolean passwordIsValid();

    String getName();

    String getPassword();
}
